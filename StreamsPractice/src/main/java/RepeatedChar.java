import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.stream.Collectors;

public class RepeatedChar {
    public static void main(String[] args) {
    	String input = "Java Hungry Blog Alive is Awesome";
    	Optional<Character> firstRepeatedChar = input.chars()
    	                .mapToObj(c -> (char) c)
    	                .collect(Collectors.toCollection(LinkedHashSet::new)).stream()
    	                .filter(c -> input.indexOf(c) != input.lastIndexOf(c))
    	                .findFirst();

    	if (firstRepeatedChar.isPresent()) {
    	    System.out.println("First repeated character: " + firstRepeatedChar.get());
    	} else {
    	    System.out.println("No repeated character found");
    	}

    }
}
