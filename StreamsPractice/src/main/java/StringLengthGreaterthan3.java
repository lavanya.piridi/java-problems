import java.util.Arrays;
import java.util.List;

public class StringLengthGreaterthan3 {
    public static void main(String[] args) {
    	 List<String> stringList = Arrays.asList("Hello","Interview","Questions","Answers","Ram","for");
         stringList.stream().filter(str -> str.length() > 3).forEach(System.out::println);
    }
}
