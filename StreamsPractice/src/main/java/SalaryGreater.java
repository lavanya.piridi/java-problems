
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Optional;

	public class SalaryGreater {
	    public static void main(String[] args) {
	        List<Employee> employeeList = new ArrayList<>();
	        employeeList.add(new Employee("Mani", 29, 50000, "Manager"));
	        employeeList.add(new Employee("Sai", 26, 45000, "Senor Developer"));
	        employeeList.add(new Employee("Manasa", 23, 30000, "Developer"));

	        Optional<Employee> firstEmployeeWithSalaryGreaterThan50000 = employeeList.stream()
	                .filter(employee -> employee.getSalary() > 50000)
	                .findFirst();

	        if (firstEmployeeWithSalaryGreaterThan50000.isPresent()) {
	            System.out.println("Employee with salary greater than 50000: " + firstEmployeeWithSalaryGreaterThan50000.get());
	        } else {
	            System.out.println("No employee found with salary greater than 50000.");
	        }
	    }
	}

	class Employee {
	    private String name;
	    private int age;
	    private long salary;
	    private String designation;

	    public Employee(String name, int age, long salary, String designation) {
	        this.name = name;
	        this.age = age;
	        this.salary = salary;
	        this.designation = designation;
	    }

	    public int getSalary() {
			// TODO Auto-generated method stub
			return 0;
		}

		// create Getters, Setters
	    

	    public String toString() {
	        return "Name: " + name + ", Age: " + age + ", Salary: " + salary + ", Designation: " + designation;
	    }

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public String getDesignation() {
			return designation;
		}

		public void setDesignation(String designation) {
			this.designation = designation;
		}

		public void setSalary(long salary) {
			this.salary = salary;
		}
	}

