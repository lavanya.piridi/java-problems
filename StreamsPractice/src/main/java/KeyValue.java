
import java.util.*;

public class KeyValue {

    private String name;
    private int age;
    private long salary;
    private String designation;

    // Constructor
    public KeyValue(String name, int age, long salary, String designation) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.designation = designation;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}

public class Main{

    public static void main(String[] args) {
        // Create a List of Employee objects
        List<Employee> employeeList = new ArrayList<>();

        employeeList.add(new Employee("Mani", 29, 50000, "Manager"));
        employeeList.add(new Employee("Sai", 26, 45000, "Senor Developer"));
        employeeList.add(new Employee("Manasa", 23, 30000, "Developer"));

        // Create a Map of employees by designation
        Map<String, List<Employee>> employeesByDesignation = new HashMap<>();

        for (Employee employee : employeeList) {
            String designation = employee.getDesignation();

            if (employeesByDesignation.containsKey(designation)) {
                employeesByDesignation.get(designation).add(employee);
            } else {
                List<Employee> employeesList = new ArrayList<>();
                employeesList.add(employee);
                employeesByDesignation.put(designation, employeesList);
            }
        }

        // Print the Map of employees by designation
        for (String designation : employeesByDesignation.keySet()) {
            System.out.println(designation + " employees: " + employeesByDesignation.get(designation));
        }
    }
}
