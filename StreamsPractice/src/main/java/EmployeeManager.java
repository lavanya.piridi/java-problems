import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeManager {
    private String name;
    private int age;
    private long salary;
    private String designation;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public long getSalary() {
		return salary;
	}
	public void setSalary(long salary) {
		this.salary = salary;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
 
	List<Employee> employees = new ArrayList<>();
	employeeList.add(new Employee("Mani", 29, 50000, "Manager"));
    employeeList.add(new Employee("Sai", 26, 45000, "Senor Developer"));
    employeeList.add(new Employee("Manasa", 23, 30000, "Developer"));

	List<Employee> managers = employees.stream()
	                                   .filter(employees -> employees.getDesignation().equals("Manager"))
	                                   .collect(Collectors.toList());

}
