import java.util.Optional;

public class NonRepeatedCharacter {
     public static void main(String[] args) {
    	 String input = "Java Hungry Blog Alive is Awesome";
    	 Optional<Character> firstNonRepeatedChar = input.chars()
    	                 .mapToObj(c -> (char) c)
    	                 .filter(c -> input.indexOf(c) == input.lastIndexOf(c))
    	                 .findFirst();

    	 if (firstNonRepeatedChar.isPresent()) {
    	     System.out.println("First non-repeated character: " + firstNonRepeatedChar.get());
    	 } else {
    	     System.out.println("No non-repeated character found");
    	 }

     }
}
