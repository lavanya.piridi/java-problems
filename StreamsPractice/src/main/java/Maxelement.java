import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Maxelement {
    public static void main(String[] args) {
    	List<String> stringList = new ArrayList<String>();

    	stringList.add("10");
    	stringList.add("15");
    	stringList.add("8");
    	stringList.add("49");
    	stringList.add("25");
    	stringList.add("98");
    	stringList.add("32");
    	stringList.add("15");
    	stringList.add("98");

    	Stream<String> stream = stringList.stream();

    	Optional<String> max = stream.max(new Comparator<String>() {
			public int compare(String val1, String val2) {
			    return val1.compareTo(val2);
			}
		});

    	String maxString = max.get();

    	System.out.println(maxString);

    }
}
