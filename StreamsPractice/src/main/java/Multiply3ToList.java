import java.util.Arrays;
import java.util.*;
import java.util.stream.Collectors;

public class Multiply3ToList {
     public static void main(String[] args) {
    	 List<Integer> integerList = Arrays.asList(1,2,3,4,5,6,7);
         System.out.println(integerList.stream().map(i -> i*3).collect(Collectors.toList()));
     }

     }
