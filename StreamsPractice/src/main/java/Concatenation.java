import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class Concatenation {
   public static void main(String[] args) {
	   List<String> stringList = new ArrayList<String>();

	   stringList.add("1,2,3,4");

	   Stream<String> stream1 = stringList.stream();

	   List<String> stringList2 = new ArrayList<String>();
	   stringList2.add("5,6,7");

	   Stream<String> stream2 = stringList2.stream();

	   Stream<String> concatStream = Stream.concat(stream1, stream2);

	   List<Object> stringsAsUppercaseList = concatStream
	           .collect(Collectors.toList());

	   System.out.println(stringsAsUppercaseList);
   }
} 
