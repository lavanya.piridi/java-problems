//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
//import java.util.function.Function;
//import java.util.stream.Stream;

public class CountElements {

	  public static void main(String[] args) {
		  List<String> stringList = new ArrayList<String>();

		  stringList.add("10");
		  stringList.add("15");
		  stringList.add("8");
		  stringList.add("49");
		  stringList.add("25");
		  stringList.add("98");
		  stringList.add("98");
		  stringList.add("32");
		  stringList.add("15");

		  Stream<String> stream = stringList.stream();

		  long count = stream.flatMap((value) -> {
		      String[] split = value.split(" ");
		      return (Stream<String>) Arrays.asList(split).stream();
		  })
		  .count();

		  System.out.println("count = " + count);

	  }
}
